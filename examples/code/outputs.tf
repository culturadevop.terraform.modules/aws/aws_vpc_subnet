output "vpc_id" {
  description = "Id of the created vpc."
  value       = module.module_usage_howto.vpc_id
}

output "vpc_arn" {
  description = "Arn of the created vpc."
  value       = module.module_usage_howto.vpc_arn
}

output "subnets_ids" {
  description = "Ids of the created subnets of VPC."
  value       = module.module_usage_howto.subnets_ids
}

output "subnets_arns" {
  description = "Arns of the created subnets of VPC."
  value       = module.module_usage_howto.subnets_arns
}


output "debug" {
  description = "For debug purpose."
  value       = module.module_usage_howto.debug
}

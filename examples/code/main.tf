provider "aws" {
  profile = "default"
  region  = "eu-west-1"
}

module "module_usage_howto" {
  source = "./../.."

  project = {
    project     = "my-project"
    environment = "dev"
    createdBy   = "markitos"
    group       = "my-group"
  }

  resources = {
    name       = "vpc-1"
    cidr_block = "10.10.0.0/16",
    subnets = [
      {
        name       = "subnet-1"
        cidr_block = "10.10.1.0/24"
        tags = {
          Subnet = "Subnet-1"
        }
        }, {
        name       = "subnet-2"
        cidr_block = "10.10.2.0/24"
        tags = {
          Subnet = "Subnet-2"
        }
        }, {
        name       = "subnet-3"
        cidr_block = "10.10.3.0/24"
        tags = {
          Subnet = "Subnet-3"
        }
    }]
    tags = {
      Name = "vpc-1"
    }
  }

}
